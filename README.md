# ofxNoise

C++ implementation of Perlin and Simplex noise for OpenFrameworks, adapted from the Java noise classes by Karsten Schmidt.

## Getting started

- Download OpenFrameworks from www.openframeworks.cc
- Create a new project
- Copy the folder to the addons folder of your project and add it to the header search path of your project
- *OPTIONAL* Replace the files in your project with the files in the example folder if you want to explore the provided example

## License
MIT license - https://opensource.org/licenses/MIT

## Roadmap
This library has not been updated in a while and will only be updated in case I require to use it on a new project (sorry..)

## Author
Rui Madeira